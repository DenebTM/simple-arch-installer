#!/bin/bash
# Check internet connection
function check_internet() {
    echo "Checking internet connection"
    ping -c 2 archlinux.org
    rc=$?
}
# simple y/n function
# specify whether to default to "y" or "n" in $1
# returns 0 for y and 1 for n
function yesno() {
    yn="gone"
    while [[ "$yn" != "y" ]] && [[ "$yn" != "n" ]]; do
        read yn; yn=$(echo "$yn" | awk '{print tolower($0)}')
        if [[ "$yn" = "y" ]] || ([[ "$yn" = "" ]] && [[ "$1" = "y" ]]); then
            return 0
        elif [[ "$yn" = "n" ]] || ([[ "$yn" = "" ]] && [[ "$1" = "n" ]]); then
            return 1
        fi
        echo "Please enter either y or n."
    done
}


# Check boot mode
if [ -d "/sys/firmware/efi" ]; then
    bootmode="efi"
    bootmodetext="EFI"
else
    bootmode="bios"
    bootmodetext="BIOS"
fi

if [ ! "$part2" = true ]; then

    ## Prerequisites
    echo; echo
    check_internet
    
    while [[ ! $rc -eq 0 ]]; do
        echo "You do not appear to be connected to the internet."
        echo "If you don't want to setup WLAN, please plug in an ethernet cable now."
        echo "Do you want to set up WLAN? (Y/n)"
        
        read wifiyn
        if yesno y; then
            iwd
        fi
        
        check_internet
    done

    echo "Connection verified!"
    echo; echo

    timedatectl set-ntp true
    loadkeys de

    lsblk
    echo; echo "Which drive will you be installing to?"
    printf "/dev/"
    read drv
    drv="/dev/$drv"

    # fdisk
    echo; echo "Set up your partitions in the next step. Keep in mind this install is going to be for $bootmodetext"
    echo "Press Enter to continue."
    read
    fdisk "$drv"
    
    lsblk

    # root partition
    echo "Which partition is going to be /?"
    printf "$drv"
    read rootpart
    rootpart="$drv$rootpart"

    echo "Format? (Y/n)"
    if yesno y; then
        mkfs.ext4 $rootpart
    fi
    mount $rootpart /mnt

    # boot partition
    if [[ "$bootmode" = "efi" ]]; then
        echo "Which partition is going to be /boot/efi?"
        printf "$drv"
        read efipart
        
        efipart="$drv$efipart"
        echo "Format? (Y/n)"
        if yesno y; then
            mkfs.fat -F32 $efipart
        fi
        mkdir -p /mnt/boot/efi
        mount $efipart /mnt/boot/efi
    else
        echo "Which partition is going to be /boot?"
        echo "Hit Return for none."
        printf "$drv"
        read bootpart
        
        if [ ! -z "$bootpart" ]; then
            bootpart="$drv$bootpart"
            echo "Format? (Y/n)"
            if yesno y; then
                mkfs.ext4 $bootpart
            fi
            mkdir /mnt/boot
            mount $bootpart /mnt/boot
        fi
    fi

    # swap partition
    echo "Which partition is going to be swap?"
    echo "Hit Return for none."
    printf "$drv"
    read swappart

    if [ ! -z "$swappart" ]; then
        swappart="$drv$swappart"
        echo "Format? (Y/n)"
        if yesno y; then
            mkswap $swappart
        fi
        swapon $swappart
    fi

    # home partition
    echo "Which partition is going to be /home?"
    echo "Hit Return for none."
    printf "$drv"
    read homepart

    if [ ! -z "$homepart" ]; then
        homepart="$drv$homepart"
        echo "Format? (y/N)"
        if yesno n; then
            mkfs.ext4 $homepart
        fi
        mkdir /mnt/home
        mount $homepart /mnt/home
    fi

    # get mirrorlist
    curl "https://www.archlinux.org/mirrorlist/?country=AT&country=DE&country=CH&protocol=http&protocol=https&ip_version=4" > /etc/pacman.d/mirrorlist
    vim /etc/pacman.d/mirrorlist

    # install
    addpkg1="man-db man-pages cifs-utils nfs-utils bash-completion networkmanager modemmanager"
    addpkg2="plasma kde-utilities dolphin juk kdeconnect plasma-browser-integration firefox modem-manager-gui"
    echo "Installation is about to commence. Do you want to install additional packages?"
    echo; echo "_1: Recommended packages: $addpkg1"
    echo; echo "_2: KDE plasma desktop: $addpkg2"
    echo "Enter _1, _2 or both (separated by spaces) to select from the above package groups."
    echo "Feel free to append additional packages, also separated by spaces."
    read pkgextra
    pkgextrabak=$pkgextra
    pkgextra=${pkgextra/_1/$addpkg1}
    pkgextra=${pkgextra/_2/$addpkg2}
    echo "Installing packages."
    pacstrap /mnt base linux linux-firmware vi vim sudo $pkgextra

    # fstab
    echo "Generating fstab..."
    genfstab -U /mnt >> /mnt/etc/fstab
    
    cp ./archinstall.sh /mnt/
    
    echo $drv > /mnt/.bootdrv

    # switch to chroot and continue install
    arch-chroot /mnt bash -c "part2=true ./archinstall.sh"
else
    drv=$(cat .bootdrv)
    
    # time and locale
    ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime
    hwclock --systohc

    vim /etc/locale.gen
    locale-gen
    echo "LANG=en_GB.UTF-8" > /etc/locale.conf
    vim /etc/locale.conf
    echo "KEYMAP=de-latin1" > /etc/vconsole.conf
    vim /etc/vconsole.conf

    # network
    printf "Enter hostname to be used: "
    read hostname
    echo "$hostname" > /etc/hostname

    cat << EOF >> /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   $hostname.localdomain	$hostname
EOF
    vim /etc/hosts

    mkinitcpio -P

    echo "Enter password to be used for root."
    passwd

    # install GRUB
    echo "Installing GRUB in $bootmodetext mode to $drv. Press Return to continue."
    read
    if [[ "$bootmode" = "efi" ]]; then
        pacman --noconfirm -S grub efibootmgr
        
        grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB-Arch
    else
        pacman --noconfirm -S grub
        grub-install --target=i386-pc $drv
    fi

    if [ ! $? -eq 0 ]; then
        echo "Something went wrong apparently; Deneb pls fix."
        echo "Or press Return to continue if all appears to be in order."
        read
    fi

    echo "Make any adjustments to the GRUB configuration file in the next step."
    read

    vim /etc/default/grub

    echo "Generating GRUB config file."
    grub-mkconfig -o /boot/grub/grub.cfg
    
    echo "It is recommended to add at least one non-root user. Do you want to do this now? (Y/n)"
    if yesno y; then
        printf "Username (no spaces, all lowercase): "
        read newusername
        useradd -mNg users -G wheel $newusername
        echo "Enter password to be used for $newusername"
        passwd $newusername
        
        visudo
    fi
    
    echo "Removing leftover files"
    rm /archinstall.sh /.bootdrv
    
    echo "Make any final adjustments, such as enabling services, then type exit or press Ctrl+D to exit."
    bash
fi

if [ ! "$part2" = true ]; then
    echo "Press Return to unmount all partitions (umount -R /mnt) and reboot."
    read
    umount -R /mnt && reboot
fi
